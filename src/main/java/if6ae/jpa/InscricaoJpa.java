package if6ae.jpa;

/**
 * Created by André on 13/12/2016.
 */
import if6ae.entity.InscricaoEntity;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class InscricaoJpa {
    @PersistenceUnit(unitName="inscricaoPU")
    private EntityManager em;


    public InscricaoEntity findByNumero(int numero) throws Exception {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<InscricaoEntity> cq = cb.createQuery(InscricaoEntity.class);
        Root<InscricaoEntity> rt = cq.from(InscricaoEntity.class);
        cq.where(cb.equal(rt.get("numero"), numero));
        TypedQuery<InscricaoEntity> q = em.createQuery(cq);
        return q.getSingleResult();

    }



    public InscricaoEntity findByCPF(int cpf) throws Exception {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<InscricaoEntity> cq = cb.createQuery(InscricaoEntity.class);
        Root<InscricaoEntity> rt = cq.from(InscricaoEntity.class);
        cq.where(cb.equal(rt.get("cpf"), cpf));
        TypedQuery<InscricaoEntity> q = em.createQuery(cq);
        return q.getSingleResult();
    }
}
