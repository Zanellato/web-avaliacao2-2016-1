package if6ae.jpa;

/**
 * Created by André on 13/12/2016.
 */

import if6ae.entity.InscricaoEntity;
import if6ae.entity.InscricaoMinicursoEntity;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class InscricaoMinicursoJpa {
    @PersistenceUnit(unitName="inscricaoPU")
    private EntityManager em;

    public List<InscricaoMinicursoEntity> findInscricaoMinicursoByNumero (int numero) throws Exception {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<InscricaoMinicursoEntity> cq = cb.createQuery(InscricaoMinicursoEntity.class);
        Root<InscricaoMinicursoEntity> rt = cq.from(InscricaoMinicursoEntity.class);
        cq.where(cb.equal(rt.get("numeroInscricao"), numero));
        TypedQuery<InscricaoMinicursoEntity> q = em.createQuery(cq);
        return q.getResultList();

    }



}
